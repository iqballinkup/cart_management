import 'package:cart_and_hive/model/product.dart';
import 'package:cart_and_hive/model/product_list.dart';
import 'package:cart_and_hive/widgets/shoping_cart_badge.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final Box box;

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    box = Hive.box('productBox');
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: const Text('Add to Cart'),
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5.0, right: 10.0),
            child: ShoppingCartBadge(),
          ),
        ],
      ),
      backgroundColor: Color.fromARGB(255, 226, 226, 226),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 300,
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: myProductList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Card(
                        elevation: 8,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            SizedBox(
                              height: 200,
                              width: 200,
                              child:
                                  Image.asset(myProductList[index]['pImage']),
                            ),
                            Row(
                              children: [
                                Text(
                                  myProductList[index]['pName'].toString(),
                                  style: TextStyle(fontSize: 18),
                                ),
                                SizedBox(
                                  width: 5.0,
                                ),
                                Text(
                                  r'$' +
                                      myProductList[index]['pPrice'].toString(),
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  bool found = false;
                                  setState(() {
                                    box.length;
                                  });
                                  for (int i = 0; i < box.length; i++) {
                                    ProductDetails existingProduct =
                                        box.getAt(i);
                                    if (existingProduct.productName ==
                                            myProductList[index]['pName'] &&
                                        existingProduct.productPrice ==
                                            myProductList[index]['pPrice']) {
                                      existingProduct.productQuantity =
                                          myProductList[index]['pQuantity'];
                                      box.putAt(i, existingProduct);

                                      found = true;
                                      break;
                                    }
                                  }
                                  if (!found) {
                                    ProductDetails productDetails =
                                        ProductDetails(
                                            productName: myProductList[index]
                                                ['pName'],
                                            productPrice: myProductList[index]
                                                ['pPrice'],
                                            productQuantity:
                                                myProductList[index]
                                                    ['pQuantity']);
                                    box.add(productDetails);
                                  }
                                },
                                child: const Text("Add to Cart"))
                          ],
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    ));
  }
}
