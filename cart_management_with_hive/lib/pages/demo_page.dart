import 'package:cart_and_hive/model/product_list.dart';
import 'package:flutter/material.dart';

//===============================================
// author: Iqbal Hossen Riaz

// This page is not relevant to the project, It was created to check demo designs, weather the designs work on not.
// you may delete this file.
//===============================================

class DemoPage extends StatefulWidget {
  const DemoPage({super.key});

  @override
  State<DemoPage> createState() => _DemoPageState();
}

class _DemoPageState extends State<DemoPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SizedBox(
          child: ListView.builder(
            itemCount: myProductList.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  Text(myProductList[index]['pName']),
                  Text('Price: ${myProductList[index]['pPrice']}'),
                  Image.asset(myProductList[index]['pImage']),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
