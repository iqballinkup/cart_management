class ProductList {
  final String? pName;
  final int? pQuantity;
  final int? pPrice;
  final String? pImage;

  ProductList({this.pName, this.pQuantity, this.pPrice, this.pImage});
}

List myProductList = [
  {
    "pQuantity": 1,
    "pPrice": 50,
    "pName": "Hair Oil",
    "pImage": "images/product1.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 100,
    "pName": "Shampoo",
    "pImage": "images/product2.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 150,
    "pName": "Lotion",
    "pImage": "images/product3.jpg"
  }
];
